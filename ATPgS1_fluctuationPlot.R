ATPgS1_fluctuationPlot <- function(treatment, ROI=c("OHC", "IHC", "KO", "Hensen")){
  #treatment = control or neo
  #ROI <- c("OHC", "IHC", "KO", "Hensen")
  source("ATPgS1_fluctuationSummary.R")
  plotData <- vector()
  statsData <- vector()
  for(i in ROI){
    data <- data.frame(ATPgS1_fluctuationSummary(treatment, i), ROI= i)
    df <- data.frame(DF, ROI=i)
    plotData <- rbind(plotData, data)
    statsData <- rbind(statsData, df)
  }
  plotData <<- plotData
  
  plot=ggplot(plotData, aes(x=ROI, y=ave)) + 
    geom_bar(position=position_dodge(), colour="black", stat="identity") +
    geom_errorbar(aes(ymin=ave, ymax=ave+sem),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    #labs(y= expression(paste(" R340/380"))) +
    labs(y= expression(paste(Delta, " R340/380"))) +
    #ylim(0, 0.12) +
    labs(x=NULL) +
    theme(axis.title=element_text(size=22, face="bold"),
          axis.text=element_text(size=20,face="bold"),
          plot.title= element_text(size=24, face="bold"),
          legend.position="none")
  
  kw <- kruskal.test(statsData$Ave_Fluctn~statsData$ROI)
  posthoc <- dunn.test(statsData$Ave_Fluctn,statsData$ROI)
  posthoc
  print(plot)
}