ReceptorActivation <- function(Treatment, ROI, T1, T2, threshold = 75, ymin=NULL, ymax=NULL, plotGraph=TRUE){
  #T1 = 0
  #T2 = 900
  #Treatment="control"
  #ROI = "KO"
  #         DF2 = ReceptorData("C10_control", 150,900)
 # DF3 =ReceptorData("C6_control", 150,600)
  
  library("ggplot2")
  library("caTools")
  
  data <- read.csv(paste0(Treatment, ".csv"))

  # Data subset according to the time defined in T1 and T2
  subdata_Time <- subset(data, Time >= T1 & Time <= T2)
  # Plots segment
  if(ROI=="Hensen"){
    CaPlot = ggplot(data= subdata_Time, aes(x=Time, y=Hensen)) 
  }else if(ROI=="KO"){
    CaPlot = ggplot(data= subdata_Time, aes(x=Time, y=KO))
  }else if(ROI=="OHC1"){
    CaPlot = ggplot(data= subdata_Time, aes(x=Time, y=OHC1))
  }else if(ROI=="OHC2"){
    CaPlot = ggplot(data= subdata_Time, aes(x=Time, y=OHC2))
  }else if(ROI=="OHC3"){
    CaPlot = ggplot(data= subdata_Time, aes(x=Time, y=OHC3))
  }else if(ROI=="IHC"){
    CaPlot = ggplot(data= subdata_Time, aes(x=Time, y=IHC))
  }
  if(!is.null(ymin) & !is.null(ymax)){
    CaPlot = CaPlot+
      geom_point(size=3.5) +
      geom_line(size=1.5) +
      labs(y=paste0("R340/380 (",ROI, ")")) +
      ylim(ymin, ymax)  +
      scale_x_continuous(name="Time (s)",breaks=c(seq(T1, T2, by=200))) +
      ggtitle(paste0(Treatment, " -- ", T1,"s to ", T2,"s")) +
      theme(axis.title=element_text(size=22, face="bold"),
            axis.text=element_text(size=20,face="bold"),
            plot.title= element_text(size=24, face="bold"))
  }else{
    CaPlot = CaPlot+
      geom_point(size=3.5) +
      geom_line(size=1.5) +
      labs(y=paste0("R340/380 (",ROI, ")")) +
      scale_x_continuous(name="Time (s)",breaks=c(seq(T1, T2, by=200))) +
      ggtitle(paste0(Treatment, " -- ", T1,"s to ", T2,"s")) +
      theme(axis.title=element_text(size=22, face="bold"),
            axis.text=element_text(size=20,face="bold"),
            plot.title= element_text(size=24, face="bold"))
  }
  if(plotGraph ==TRUE){
    print(CaPlot)  
  }
  
  # Gradient calculation
  Time <- subdata_Time$Time
  ROIdata <- subdata_Time[[ROI]] 
  grad1 <- diff(ROIdata)/diff(Time) #gradient
  grad1.summary = summary(abs(grad1))
  median.gradient = grad1.summary[["Median"]]

  GradInfo <- data.frame(Time[-1],grad1>median.gradient, grad1>0)
  colnames(GradInfo)<- c("Time", "Steep", "Positive")
  # Steep = slop steepness arbitrary defined 
  if(ROI=="Hensen"){
    Maximum <- subset(subdata_Time,ROIdata == max(ROIdata), select = c(Time,Hensen)) 
  }else if(ROI=="KO"){
    Maximum <- subset(subdata_Time,ROIdata == max(ROIdata), select = c(Time,KO)) 
  }else if(ROI=="OHC1"){
    Maximum <- subset(subdata_Time,ROIdata == max(ROIdata), select = c(Time,OHC1)) 
  }else if(ROI=="OHC2"){
    Maximum <- subset(subdata_Time,ROIdata == max(ROIdata), select = c(Time,OHC2)) 
  }else if(ROI=="OHC3"){
    Maximum <- subset(subdata_Time,ROIdata == max(ROIdata), select = c(Time,OHC3)) 
  }else if(ROI=="IHC"){
    Maximum <- subset(subdata_Time,ROIdata == max(ROIdata), select = c(Time,IHC)) 
  }


  # --------------- onset time calculation ---------------------------------------------------------------------------
 
    PossibleOnsets <- subset(GradInfo, GradInfo$Steep==T & GradInfo$Positive==T & GradInfo$Time < Maximum$Time)

    #PossibleOnsets <- subset(GradInfo, GradInfo$Positive==T & GradInfo$Time < Maximum$Time)

  
   if(length(PossibleOnsets[,1])!=0|!all(PossibleOnsets$Time_consec ==FALSE)){
    # Find all the points that have steep gradient and positive slope and is earlier than the peak
    Time_consec <- diff(PossibleOnsets$Time)<=10
    Time_consec <- append(Time_consec,FALSE)
    # Check for consecuctive values 
    PossibleOnsets <- data.frame(PossibleOnsets,Time_consec)
    
    l=length(PossibleOnsets$Time_consec)
    i=1
    max=0
    k=0
    for(i in 1:l){
      count=0
      if(PossibleOnsets$Time_consec[i]==F){
        i=i+1
      }else{
        first = i
        while(PossibleOnsets$Time_consec[i]==T){
          count=count+1
          i = i+1
        } 
        if(max < count){
          max=count
          k=first
        }
      }
    }
    # Finds the index (k) where there is the most consecutive TRUE and max = number of consecutive TRUE
    Onset_Time <- PossibleOnsets[k,"Time"] -10
    # Take the Time at k the point beofre that as the onset time
   }else{
     Onset_Time=vector()
  }
  
  if(length(Onset_Time)==0){
    Onset_Time = NA
    Baseline=mean(subdata_Time[[ROI]])
  }else{
    # Baseline
    #BaselineData <- subset(subdata_Time, Time >= ((T2-T1)/6 + T1) & Time <= ((T2-T1)/3+T1))
    B1 <- (Onset_Time - T1)/2 + T1
    B2 <- Onset_Time - 10
    BaselineData <- subset(subdata_Time, Time >= B1 & Time <= B2)
    Baseline <- mean(BaselineData[[ROI]])
  }
  

  # --------------- End time calculation ---------------------------------------------------------------------------
  EndData<-data.frame(Time, subdata_Time[[ROI]])
  colnames(EndData) <- c("Time","ROI")
  EndData <- subset(EndData, EndData$Time > Maximum$Time)
  t=threshold/100
  amplitude = Maximum[,2]-Baseline
  cutoff = Maximum[,2]-t*amplitude
  t_index = which(abs(EndData$ROI-cutoff)==min(abs(EndData$ROI-cutoff))) # not reliable -- need to find point closest to cut off but within close range to the peak
  End_Time_ROI = EndData[t_index,]
  if(nrow(End_Time_ROI)==0){
    End_Time=NA
  }else{
    End_Time = End_Time_ROI$Time
  }
  
  t_index_100 = which(abs(EndData$ROI-Baseline)==min(abs(EndData$ROI-Baseline)))
  End_Time_ROI100 = EndData[t_index_100,]
  if(nrow(End_Time_ROI100)==0){
    End_Time_100=NA
  }else{
    End_Time_100 = End_Time_ROI100$Time
  }

  #Area under curve
  AoCdata <- subset(subdata_Time, Time >= Onset_Time & Time <= End_Time_100)
  if(nrow(AoCdata)==0){
    AoC=NA
  }else{
  AoC <- trapz(AoCdata$Time, AoCdata[[ROI]])
  }
  
  #Rise and Recovery time
  riseTime <- Maximum$Time - Onset_Time
  recoveryTime <- End_Time - Maximum$Time   
  
  #if(length(PossibleOnsets[,1])==0){
  #  #print("Blah...")
  #  Results <- cbind(NA,NA,NA,NA,NA,NA,NA)
  #  Results <- as.data.frame(Results)
  #  colnames(Results) <- c("Onset_Time", "End_Time", "Duration", "Baseline", "Peak", "Amplitude", "Area")
  #}else if(all(PossibleOnsets$Time_consec ==FALSE)){
  #  #print("No consecutive rise in Ca2+ detected")
  #  Results <- cbind(NA,NA,NA,NA,NA,NA,NA)
  #  Results <- as.data.frame(Results)
  #  colnames(Results) <- c("Onset_Time", "End_Time", "Duration", "Baseline", "Peak", "Amplitude", "Area")
  #}else{
    Duration <- End_Time - Onset_Time
    Results <- data.frame(Onset_Time, End_Time, Duration, round(Baseline,4), round(Maximum[1,2],4), round(Maximum[1,2]-Baseline,4), round(AoC,0))
    colnames(Results) <- c("Onset_Time", "End_Time", "Duration", "Baseline", "Peak", "Amplitude", "Area")
    ChangeTime <- data.frame(riseTime, recoveryTime)
    Results <- data.frame(Results, ChangeTime)
  #}
 
  return(Results)
  
}
