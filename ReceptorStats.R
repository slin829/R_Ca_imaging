ReceptorStats<- function(conc, feature, cell.type, smooth =NULL){
  source("getReceptorDF.R")
  
  DF_all= getReceptorDF(conc, smooth)
  stats <-vector()
  for(treatment in c("control", "neo")){
    DF <- subset(DF_all, DF_all$Treatment %in% c(treatment))
    ave= mean(subset(DF[[feature]], DF$Cell.Type == cell.type),na.rm=T)
    std = sd(subset(DF[[feature]], DF$Cell.Type == cell.type),na.rm=T)
    n=  sum(!is.na(subset(DF[[feature]], DF$Cell.Type == cell.type)))
    sem=std/sqrt(n)
    summary <- data.frame(ave, std, n,sem)
    stats <- rbind(stats, summary)
  }

  stats <- cbind(c("control", "neo"),stats)
  colnames(stats) <- c("Treatment", "ave", "std", "n", "sem")
  
  stats
  #barx <- barplot(stats$ave, 
  #                names.arg = row.names(stats),
  #                xlab = "Treatment",
  #                ylab = feature,
  #                ylim = c(0, max(stats$ave, na.rm=T) + max(stats$sem,na.rm=T) ))

  
  #arrows(barx, stats$ave+stats$sem, barx, stats$ave, angle = 90, code=1,length=0.1, lwd = 1)   
  
}