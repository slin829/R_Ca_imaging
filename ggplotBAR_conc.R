
BARplot_conc <- function(feature, cell.type, smooth=NULL){
  source("ReceptorStats.R")
  if(!feature %in% c("Duration","Baseline", "Peak", "Amplitude", "Area","riseTime","recoveryTime")){
    stop("feature options are Duration, Baseline, Peak, Amplitude, Area")
  }
  if(!cell.type %in% c("IHC", "OHC1", "OHC2", "OHC3", "KO", "Hensen")){
    stop("ROI options are IHC, OHC1, OHC2, OHC3, KO, Hensen")
  }
  DF <- vector()
  #conc = c("ATPgS1","ATPgS5","ATPgS10","ATP10","ATP100" )
  conc = c("ATPgS1","ATPgS10")
  
  for(c in conc) {
    result<- ReceptorStats(c, feature, cell.type, smooth)
    result2 <- cbind(result, rep(c)) 
    DF <- rbind(DF, result2)
  }
  colnames(DF) <- c("Treatment", "ave", "std", "n", "sem", "Conc")
  DF <<- DF
  
  ggplot(DF, aes(x=Conc, y=ave, fill=Treatment)) + 
    geom_bar(position=position_dodge(), colour="black", stat="identity") +
    geom_errorbar(aes(ymin=ave, ymax=ave+sem),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    labs(y = feature)+
    labs(x=NULL)+
    ggtitle(paste(cell.type, smooth))
  
} 


