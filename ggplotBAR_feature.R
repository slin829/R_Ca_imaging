
BARplot_feature<- function(conc, cell.type, smooth=NULL){
  source("ReceptorStats.R")
  library("gridExtra")
  
  if(!conc %in% c("ATPgS1","ATPgS5","ATPgS10","ATP10","ATP100","ATP100_rep")){
    stop("conc options are ATPgS1, ATPgS5, ATPgS10, ATP10, ATP100 or ATP100_rep")
  }

  
  #plot1
  DF <- vector()
  #feature1=c("Duration", "Area")
  feature1= "Baseline"
  for(f in feature1) {
    result<- ReceptorStats(conc, f, cell.type, smooth)
    result2 <- cbind(result, rep(f)) 
    DF <- rbind(DF, result2)
  }
  colnames(DF) <- c("Treatment", "ave", "std", "n", "sem", "Feature")
  DF <<- DF
  #plot2
  DF2 <- vector()
  #feature2= c("Baseline", "Peak", "Amplitude")
  feature2="Amplitude"
  for(f in feature2) {
    result<- ReceptorStats(conc, f, cell.type, smooth)
    result2 <- cbind(result, rep(f)) 
    DF2 <- rbind(DF2, result2)
  }
  colnames(DF2) <- c("Treatment", "ave", "std", "n", "sem", "Feature")
  DF2 <<- DF2
  
  p1 = ggplot(DF, aes(x=Feature, y=ave, fill=Treatment)) + 
    geom_bar(position=position_dodge(), colour="black", stat="identity") +
    geom_errorbar(aes(ymin=ave, ymax=ave+sem),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    labs(y = "R340/R380")+
    labs(x=NULL)+ 
    theme(axis.title=element_text(size=22, face="bold"),
        axis.text=element_text(size=20,face="bold"),
        plot.title= element_text(size=24, face="bold"),
        legend.position="none")  
  
  p2= ggplot(DF2, aes(x=Feature, y=ave, fill=Treatment)) + 
    geom_bar(position=position_dodge(), colour="black", stat="identity") +
    geom_errorbar(aes(ymin=ave, ymax=ave+sem),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    #labs(y = "R340/380")+
    labs(y= expression(paste(Delta, " R340/380"))) +
    labs(x=NULL)+ 
    ylim(0,0.4) +
    theme(axis.title=element_text(size=22, face="bold"),
          axis.text=element_text(size=20,face="bold"),
          plot.title= element_text(size=24, face="bold"),
          legend.position="none")  
  
  grid.arrange(p1, p2, ncol=2)
  
} 