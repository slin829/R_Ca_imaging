# ATPgS 1uM
ReceptorActivation("C6_control","Hensen",100,600,threshold=75)
ReceptorActivation("C7_control","Hensen",250,900,threshold=75)
ReceptorActivation("C10_control","Hensen",250,900,threshold=75, 0.80,0.95)

ReceptorActivation("C8_neo","Hensen",250,900,threshold=75) #not reliable
ReceptorActivation("C9_M1","Hensen",250,900,threshold=75)
ReceptorActivation("C9_M2","Hensen",250,900,threshold=75)
ReceptorActivation("C10_neo","Hensen",250,900,threshold=75,0.80,0.95)

# ATPgS 5uM
ReceptorActivation("C6_control","Hensen",960,1600,threshold=75)
ReceptorActivation("C7_control","Hensen",800,1500,threshold=75)
ReceptorActivation("C10_control","Hensen",800,1500,threshold=75, 0.8,0.95)

ReceptorActivation("C8_neo","Hensen",800,1500,threshold=75) #not reliable
ReceptorActivation("C9_M1","Hensen",800,1500,threshold=75)
ReceptorActivation("C9_M2","Hensen",800,1500,threshold=75)
ReceptorActivation("C10_neo","Hensen",800,1500,threshold=75,0.8,0.95)

# ATPgS 10uM
ReceptorActivation("C6_control","Hensen",1500,2100,threshold=75)
ReceptorActivation("C7_control","Hensen",1400,2100,threshold=75)
ReceptorActivation("C10_control","Hensen",1400,2100,threshold=75, 0.80,0.95)

ReceptorActivation("C8_neo","Hensen",1400,2100,threshold=75) #not reliable
ReceptorActivation("C9_M1","Hensen",1400,2100,threshold=75)
ReceptorActivation("C9_M2","Hensen",1400,2100,threshold=75)
ReceptorActivation("C10_neo","Hensen",1400,2100,threshold=75,0.80,0.95)

#ATP 10uM
ReceptorActivation("C7_control","Hensen",2000,2700,threshold=75)
ReceptorActivation("C10_control","Hensen",2000,2700,threshold=75)

ReceptorActivation("C8_neo","Hensen",2000,2700,threshold=75)
ReceptorActivation("C9_M1","Hensen",2000,2700,threshold=75)
ReceptorActivation("C9_M2","Hensen",2000,2700,threshold=75)
ReceptorActivation("C10_neo","Hensen",2000,2700,threshold=75)

# ATP 100uM -1
ReceptorActivation("C6_control","Hensen",2400,3050,threshold=75)
ReceptorActivation("C3_cochlea1","Hensen",0,780,threshold=75)
ReceptorActivation("C7_control","Hensen",3500,4350,threshold=75) ## double check the conc of this one
ReceptorActivation("C10_control","Hensen",2400,3300,threshold=75)

ReceptorActivation("C8_neo","Hensen",2400,3300,threshold=75)
ReceptorActivation("C9_M1","Hensen",2400,3300,threshold=75)
ReceptorActivation("C9_M2","Hensen",2400,3300,threshold=75)
ReceptorActivation("C10_neo","Hensen",2400,3300,threshold=75)

# ATP 100uM -2
ReceptorActivation("C10_control","Hensen",3000,3890,threshold=75)

ReceptorActivation("C8_neo","Hensen",3000,3890,threshold=75)
ReceptorActivation("C9_M1","Hensen",3000,3890,threshold=75)
ReceptorActivation("C9_M2","Hensen",3000,3890,threshold=75)
ReceptorActivation("C10_neo","Hensen",3000,3890,threshold=75)
